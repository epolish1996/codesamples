<?php

namespace TestSecond\Service\Resource;

/**
 * Class ServicePageIterator
 * Lazy loading paging-iterator of service items
 */
class ServicePageIterator extends ServiceIterator
{
    public function key()
    {
        return $this->getPage();
    }

    public function valid()
    {
        return !is_null($this->getPage());
    }

    public function current()
    {
        return $this->array;
    }

    public function next()
    {
        $this->setPage($this->getPage() + 1);

        $this->array = $this->getEntities();
    }

    public function rewind()
    {
        $this->setPage($this->getPage() ?: 1);

        $this->array = $this->getEntities();
    }
}
