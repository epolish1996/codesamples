<?php

namespace TestSecond\Service\Resource;

use \Iterator;
use \TestSecond\Entity\EnumAlias;
use \TestSecond\Entity\EntityFactory;

/**
 * Class ServiceIterator
 * Lazy loading iterator of service items
 */
class ServiceIterator implements Iterator
{
    protected $page;

    protected $size;

    protected $type;

    protected $array;

    protected $client;

    protected $filters;

    public function __construct(Client $client, EnumAlias $type)
    {
        $this->client = $client;

        $this->setSize(100);
        $this->setType($type);
        $this->setFilters([]);
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType(EnumAlias $type)
    {
        $this->type = $type;
    }

    public function getFilters()
    {
        return $this->filters;
    }

    public function setFilters($filters)
    {
        $this->filters = $filters;
    }

    public function current()
    {
        return current($this->array);
    }

    public function next()
    {
        return next($this->array);
    }

    public function key()
    {
        return ($this->getPage() - 1) * $this->getSize() + key($this->array);
    }

    public function rewind()
    {
        $this->setPage(0);

        $this->array = [];

        return reset($this->array);
    }

    public function valid()
    {
        if (!$this->array || $this->current() === false) {
            $this->setPage($this->getPage() + 1);

            $this->array = $this->getEntities();
        }

        return !!$this->array;
    }

    public function getPage()
    {
        return $this->page;
    }

    public function setPage($page)
    {
        $this->page = $page;
    }

    public function getPreparedRawEntities($rawEntities)
    {
        $entities = [];
        $rawEntities = json_decode($rawEntities, true);

        foreach ($rawEntities as $rawEntity) {
            $entities[] = EntityFactory::create($this->type, $rawEntity);
        }

        return $entities;
    }

    public function getEntities()
    {
        $rawEntities = $this->client->get($this->type, array_merge([
            'page' => $this->getPage(),
            'size' => $this->getSize(),
        ], $this->getFilters()));

        if ($rawEntities == '[]') {
            $entities = [];

            $this->setPage(null);
        } else {
            $entities = $this->getPreparedRawEntities($rawEntities);
        }

        return $entities;
    }
}
