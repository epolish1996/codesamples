<?php

namespace Test\Service\Resource;

use \Exception;

/**
 * Class Client
 * Connection class to the test service
 */
class Client
{
    private $config;

    public function getConfig() : Config
    {
        return $this->config;
    }

    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    public function get($options = [])
    {
        return $this->getMappedResponseContent(
            $this->getResponse($this->getPreparedRequest($options))
        );
    }

    public function getPreparedMultiRequestGenerator($multi_options = [])
    {
        $curls = [];
        $running = null;
        $multi_curl = curl_multi_init();

        foreach ($multi_options as $options) {
            $curl =  $this->getPreparedRequest($options);

            curl_multi_add_handle($multi_curl, $curl);

            $curls[] = $curl;
        }

        do {
            curl_multi_exec($multi_curl, $running);

            $info = curl_multi_info_read($multi_curl);

            if (is_array($info) && ( $curl = $info['handle'] )) {
                $this->validateResponseMultiCode(curl_getinfo($curl, CURLINFO_HTTP_CODE));

                $response = curl_multi_getcontent($curl);

                yield $this->getMappedResponseContent($response);
            }

        } while ($running);

        foreach ($curls as $curl) {
            curl_multi_remove_handle($multi_curl, $curl);
            curl_close($curl);
        }

        curl_multi_close($multi_curl);
    }

    private function getMappedResponseContent($content)
    {
        return json_decode($content, true);
    }

    private function validateResponseMultiCode($code)
    {
        if ($code != 400) {
            $this->validateResponseCode($code);
        }
    }

    private function getUrl() : string
    {
        $host = $this->getConfig()->getHost();
        $prefix = $this->getConfig()->getPrefix();
        $version = $this->getConfig()->getVersion();
        $service = $this->getConfig()->getService();

        return "$host/$prefix/$version/$service";
    }

    private function getPreparedRequest($options = [])
    {
        $queryString = '?' . ($options ? http_build_query($options) : '');
        $curl = curl_init($this->getUrl() . $queryString);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeaders());

        return $curl;
    }

    private function getHeaders()
    {
        return [
            'Accept: application/json',
            'Token: ' . $this->getConfig()->getToken(),
            'SecureUrl: ' . $this->getConfig()->getSecureUrl(),
            'PrivateKey: ' . $this->getConfig()->getPrivateKey(),
            'Content-Type: application/json;charset=UTF-8',
        ];
    }

    private function validateResponseCode($code)
    {
        $message = '';

        if (array_key_exists($code, $this->getResponseStatuses())) {
            if ($code >= 400) {
                $message = $this->getResponseStatuses()[$code];
            }
        } else {
            $message = 'Undefined code';
        }

        if ($message) {
            throw new Exception("TestService: $message.", $code);
        }
    }

    private function getResponse($curl)
    {
        $response = curl_exec($curl);

        if (!curl_errno($curl)) {
            $this->validateResponseCode(curl_getinfo($curl, CURLINFO_HTTP_CODE));
        } else {
            throw new Exception("TestService: service error - " . $response ?: 'no content to display');
        }

        curl_close($curl);

        return $response;
    }

    private function getResponseStatuses()
    {
        return [
            201 => 'A JSON array will be returned. See below.',
            200 => 'A JSON array will be returned. See below.',
            206 => 'Partial update. Some items within the request failed to process. 
                A JSON array will be returned for each item that was sent in the update request (See below for array details). 
                An individual resopnse code will be returned for each item.',
            401 => 'Unauthorized.',
            404 => 'No response content will be received, however, this means the item being updated in the request was not found in the database.',
            400 => 'No response content will be received, however, this means the JSON object sent was invalid for the service requested.',
            415 => 'Unsupported media type. Please check the Content-type header value to make sure it is a valid XML or JSON content type.',
            500 => 'No response content will be received, however, this means an internal server error occured. 
                If the problem continues, contact Technical Support.'
        ];
    }

}
